<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%check_list_counts}}`.
 */
class m200810_120355_create_check_list_counts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%check_list_counts}}', [
            'user_id' => $this->integer(),
            'check_list_count' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%check_list_counts}}');
    }
}

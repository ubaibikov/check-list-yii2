<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%points}}`.
 */
class m200810_115938_create_points_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%points}}', [
            'user_id' => $this->integer(),
            'check_list_id' => $this->integer(),
            'title' => $this->string(55),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%points}}');
    }
}

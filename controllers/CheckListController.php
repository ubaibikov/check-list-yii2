<?php

namespace app\controllers;

use app\models\CheckList;
use Yii;
use yii\rest\Controller;

class CheckListController extends Controller
{
    public function actionIndex($userId)
    {
        $checkLists = CheckList::find()
            ->where(['user_id' => 32])
            ->all();

        if(!empty($checkLists)){
            return $this->asJson($checkLists);
        }

        return $this->asJson('CheckList empty ' . $userId);
    }

    public function actionCreate($userId)
    {

        if(Yii::$app->request->isPost){
            $model = new CheckList;
            
            $model->load(Yii::$app->request->post());
            
            if($model->validate()){
                return $this->asJson('dont habve problem');
            }

            //  $createCheckList = CheckList::check
            return $this->asJson(Yii::$app->request->post('title'));
        }

        return $this->asJson($userId);
    }
}


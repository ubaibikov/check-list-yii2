<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "check_lists".
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $title
 */
class CheckList extends \yii\db\ActiveRecord
{
    public $userId;
    public $title;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'check_lists';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'] ,'required'],
            [['user_id'], 'integer'],
            [['title'], 'string', 'max' => 55],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'title' => 'Title',
        ];
    }
}
